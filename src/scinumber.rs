use core::fmt::{self, Display};


/// Used to represent a number in the scientific notation format
/// used by the MKS 979B. Not abstracted in a different crate as the
/// formats used in many intruments differ in a considerable manner
#[derive(Debug, Copy, Clone, PartialEq)]
pub struct SciNumber {
    pub integer: u8,
    pub decimal: u8,
    pub exponent_sign: Sign,
    pub exponent: u8,
}

#[derive(Debug, Copy, Clone, PartialEq)]
pub enum Sign {
    Plus,
    Minus,
}

impl TryFrom<u8> for Sign {
    type Error = SciNumberError;
    fn try_from(sign: u8) -> Result<Self, Self::Error> {
        match sign {
            b'+' => Ok(Sign::Plus),
            b'-' => Ok(Sign::Minus),
            _ => Err(SciNumberError),
        }
    }
}
impl From<Sign> for u8{
    fn from(s: Sign) -> Self {
        match s{
            Sign::Plus => b'+',
            Sign::Minus => b'-',
        }
    }
}

impl Display for Sign {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Sign::Plus => write!(f, "+"),
            Sign::Minus => write!(f, "-"),
        }
    }
}

#[derive(Debug)]
pub struct SciNumberError;
impl SciNumber {
    /// Tries to build a number that is valid for the instrument.
    pub fn try_new(
        integer: u8,
        decimal: u8,
        exponent_sign: Sign,
        exponent: u8,
    ) -> Result<Self, SciNumberError> {
        if integer < 10 && decimal < 100 && exponent < 11 {
            Ok(SciNumber {
                integer,
                decimal,
                exponent_sign,
                exponent,
            })
        } else {
            Err(SciNumberError)
        }
    }
    pub fn into_u32(x: SciNumber) -> u32 {
        (x.integer as u32) << 24
            | (x.decimal as u32) << 16
            | (x.exponent_sign as u32) << 8
            | (x.exponent as u32)
    }
    pub fn from_ascii(s: &[u8]) -> Result<Self, SciNumberError> {
        let len = s.len();
        if len < 7 {
            Err(SciNumberError)
        } else {
            for (i, ch) in s.iter().enumerate().take(len) {
                if i == 1 || i == 4 || i == 5 {
                    continue;
                }
                if *ch < b'0' || *ch > b'9' {
                    return Err(SciNumberError);
                }
            }
            let exponent = if s.len() < 8 {
                s[6] - b'0'
            } else {
                (s[6] - b'0') * 10 + s[7] - b'0'
            };
            Ok(Self {
                integer: s[0] - b'0',
                decimal: (s[2] - b'0') * 10 + s[3] - b'0',
                exponent_sign: s[5].try_into()?,
                exponent,
            })
        }
    }
}
impl fmt::Display for SciNumber {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "{:01}.{:02}E{}{:01}",
            self.integer, self.decimal, self.exponent_sign, self.exponent,
        )
    }
}


