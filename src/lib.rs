#![no_std]

pub mod cmd;
pub mod errors;
pub mod fsm;
pub mod scinumber;
use embedded_hal::serial;

/// Maximum lenght of a complete response message from the instrument
pub const MAX_STR_LEN: usize = 20;

/// Lenght of the Response code (ACK or NAK)
pub const ACKNAK_LEN: usize = 3;

/// Lenght of the address string (such as 253)
pub const ADDR_LEN: usize = 3;

pub trait Serial: serial::Write<u8> + serial::Read<u8> {}
impl<T: serial::Read<u8> + serial::Write<u8>> Serial for T {}
pub type ReadError<S> = <S as serial::Read<u8>>::Error;
pub type WriteError<S> = <S as serial::Write<u8>>::Error;
