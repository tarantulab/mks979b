/// Used to represent diferent error states during operation.
/// Errors sent by the transducer are wrapped with the Mks979bError Value.
#[non_exhaustive]
#[derive(Debug)]
pub enum DriverError<ReadError, WriteError> {
    InitializationError,
    FormattingError,
    NotReadyToSend,
    BadMessage,
    SerialRead(ReadError),
    SerialWrite(WriteError),
    MessageBufferFull,
}

impl<ReadError, WriteError> From<core::fmt::Error>
    for DriverError<ReadError, WriteError>
{
    fn from(_err: core::fmt::Error) -> Self {
        DriverError::<ReadError, WriteError>::FormattingError
    }
}

pub enum ResponseError {
    Mks979bError(Mks979bError),
    FailedToParseResponse,
    IllegalState,
}

/// Types of errors that the instrument communicates trough error codes in response
/// messages. `UnknownError` has no corresponding code in the manual, it is there
/// to catch possible illegal states produced by malfunctions in communication
/// (like a failure in the serial port/connection)
#[derive(Debug, PartialEq, Eq)]
pub enum Mks979bError {
    UnrecognizedMessage,
    InvalidArgument,
    ValueOutOfRange,
    CommandValueCharacterInvalid,
    ControlSetpointEnabled,
    WriteToNonVolatileMemoryFailed,
    ReadFromNonVolatileMemoryFailed,
    NotInMeasurePressureMode,
    PressureTooHighForDegas,
    CalibrationIncomplete,
    NotInCalibrationMode,
    WriteToEEFail,
    ReadFromEEFail,
    UnknownError,
}
impl Mks979bError {
    /// Gets the error type communicated by the device based on the error code
    pub fn get_error(error_code: usize) -> Mks979bError {
        match error_code {
            160 => Mks979bError::UnrecognizedMessage,
            169 => Mks979bError::InvalidArgument,
            172 => Mks979bError::ValueOutOfRange,
            175 => Mks979bError::CommandValueCharacterInvalid,
            195 => Mks979bError::ControlSetpointEnabled,
            196 => Mks979bError::WriteToNonVolatileMemoryFailed,
            197 => Mks979bError::ReadFromNonVolatileMemoryFailed,
            198 => Mks979bError::NotInMeasurePressureMode,
            199 => Mks979bError::PressureTooHighForDegas,
            100..=115 => Mks979bError::CalibrationIncomplete,
            178 => Mks979bError::NotInCalibrationMode,
            300..=399 => Mks979bError::WriteToEEFail,
            400..=499 => Mks979bError::ReadFromEEFail,
            _ => Mks979bError::UnknownError,
        }
    }
}

/// Obtains the error code number from a string represented as a byte array
pub fn try_code_from_bytes(code: &[u8]) -> Result<usize, Mks979bError> {
    let mut res: usize = 0;
    if code.len() < 3 {
        Err(Mks979bError::UnknownError)
    } else {
        res += (code[0] - b'0') as usize * 100;
        res += (code[1] - b'0') as usize * 10;
        res += (code[2] - b'0') as usize;
        Ok(res)
    }
}
