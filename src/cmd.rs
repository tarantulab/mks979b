use core::fmt::{self, Display, Formatter};
use core::str::from_utf8;
use heapless::Vec;

use crate::{scinumber::SciNumber, MAX_STR_LEN};

/// Filaments for pressure sensing in the instrument.
#[derive(Copy, Clone, Debug)]
pub enum Filament {
    One,
    Two,
}

/// Digital-to-Analog converters
#[derive(Copy, Clone, Debug)]
pub enum DAC {
    DAC1, //0.5V per decade
    DAC2, //0.75V per decade
}
/// Valid baudrates for serial communications
#[derive(Copy, Clone, Debug)]
pub enum BRValue {
    X4800,
    X9600,
    X19200,
    X38400,
    X57600,
    X115200,
}
impl BRValue {
    pub fn into_integer(&self) -> u32 {
        match &self {
            BRValue::X4800 => 4800,
            BRValue::X9600 => 9600,
            BRValue::X19200 => 19200,
            BRValue::X38400 => 38400,
            BRValue::X57600 => 57600,
            BRValue::X115200 => 115200,
        }
    }
}
impl Display for BRValue {
    fn fmt(&self, formatter: &mut Formatter<'_>) -> fmt::Result {
        write!(formatter, "{}", &self.into_integer())
    }
}
/// Emission Current values
#[derive(Copy, Clone, Debug)]
pub enum ECValue {
    Manual20uA,
    Auto,
    Auto1mA,  //response only
    Auto20uA, //response only
}
/// Available pressure measurement units
#[derive(Copy, Clone, Debug)]
pub enum PUnit {
    Torr,
    MiliBar,
    Pascal,
}
/// SetPoints in the firmware of the sensor
#[derive(Copy, Clone, Debug)]
pub enum SetPoint {
    SP1,
    SP2,
    SP3,
    ControluP,  //doesn't allow config
    Protection, //doesn't allow config
}
/// Gas types for which the is a known correction factor
#[derive(Copy, Clone, Debug)]
pub enum GasType {
    //use GT
    Nitrogen, //default //1.00
    Air,      //1.00
    Argon,    //1.29
    Hydrogen, //0.46
    Helium,   //0.18
    H2O,      //1.12
    //use GC
    CarbonDioxide,      //1.42,
    Deuterium,          //0.35,
    Krypton,            //1.94
    Neon,               //0.30
    NitrogenOxide,      //1.16
    Oxigen,             //1.01
    SulfurHexaFlouride, //2.50
    Xenon,              //2.87
}

impl GasType {
    /// Obtains the Gas Correction value based on the
    /// type of gas
    pub fn into_value(&self) -> f32 {
        match self {
            GasType::Nitrogen => 1.00,
            GasType::Air => 1.00,
            GasType::Argon => 1.29,
            GasType::Hydrogen => 0.46,
            GasType::Helium => 0.18,
            GasType::H2O => 1.12,
            GasType::CarbonDioxide => 1.42,
            GasType::Deuterium => 0.35,
            GasType::Krypton => 1.94,
            GasType::Neon => 0.30,
            GasType::NitrogenOxide => 1.16,
            GasType::Oxigen => 1.01,
            GasType::SulfurHexaFlouride => 2.50,
            GasType::Xenon => 2.87,
        }
    }
}
impl Display for GasType {
    fn fmt(&self, formatter: &mut Formatter<'_>) -> fmt::Result {
        write!(formatter, "{:.2}", &self.into_value())
    }
}

#[derive(Copy, Clone, Debug)]
pub enum DeviceStatus {
    /// Filament fault, filament cannot turn on
    FilamentFault,
    /// Hot cathode is on
    HotCathodeOn,
    /// Pressure Fault. System Pressure is above the protect pressure
    PressureFault,
    /// Hot cathode is turning on. Pressure Reading will be invalid
    PressureReadingInvalid,
    /// Currently Degassing
    DegasOn,
    /// No errors found
    Ok,
}
impl From<u8> for DeviceStatus {
    fn from(ch: u8) -> Self {
        match ch {
            b'F' => DeviceStatus::FilamentFault,
            b'G' => DeviceStatus::HotCathodeOn,
            b'P' => DeviceStatus::PressureFault,
            b'W' => DeviceStatus::PressureReadingInvalid,
            b'D' => DeviceStatus::DegasOn,
            _ => DeviceStatus::Ok,
        }
    }
}

/// Values that can be queried from the instrument
#[derive(Copy, Clone, Debug)]
pub enum Value {
    //Setup Queries
    ActiveFilament,  //AF
    DeviceAdress,    //DA
    BaudRate,        //BR
    AnalogOutput,    //DAC
    EmissionCurrent, //EC
    RSDelayON,       //RSD
    TestLedON,       //TST
    PressureUnit,    //U
    UserTag,         //UT
    //Status Queries
    DeviceType,                 //DT
    ActiveFilamentON,           //FS
    FirmwareVersion,            //FV
    HardwareVersionMicroPirani, //HV
    Manufacturer,               //MF
    Model,                      //MD,
    SerialNumber,               //SN
    DeviceUptime,               //TIM1
    FilamentUptime,             //TIM2
    DeviceStatus,               //T
    OnChipSensorTemp,           //TEM1 - returns in Celsius
    HotCathodeTemp,             //TEM2 - returns in Celcius? MUST TEST!!!!
    //Pressure Measurement and Degas Commands
    ActiveFilamentPowerON,      //FP
    DegasON,                    //DG
    PressureReadingMicroPirani, //PR1, reads up to 1e-3 Torr
    PressureReadingHotCathode,  //PR2, read from 1e-4 Torr and below
    PressureReadingCombined,    //PR3, absolute pressure reading,
    //SetPointConfig(SetPoint),   //SSx, SPx, SHx, SDx
    SetPointEnabled(SetPoint), //ENx + ENC + PRO
    SetPointValue(SetPoint),
    SetPointHysteresis(SetPoint), //hysteresis must be consistent with direction
    SetPointDirection(SetPoint),
    SetPointSetOrClear(SetPoint), //SSx, clear or set
    ConfiguredGasType,
    ConfiguredGasCorrection,
}

/// Types of messages/transactions
#[derive(Clone, Debug)]
pub enum Message {
    Value(Value),
    Setting(Setting),
    None,
}
impl Display for Message {
    fn fmt(&self, formatter: &mut Formatter<'_>) -> fmt::Result {
        match self {
            Message::Value(value) => write!(formatter, "{}", value),
            Message::Setting(setting) => write!(formatter, "{}", setting),
            Message::None => write!(formatter, "404"),
        }
    }
}
impl From<Setting> for Message {
    fn from(setting: Setting) -> Self {
        Message::Setting(setting)
    }
}
impl From<Value> for Message {
    fn from(value: Value) -> Self {
        Message::Value(value)
    }
}

/// Settings in the instrument that can be modified by the user
#[derive(Clone, Debug)]
pub enum Setting {
    //Setup Commands
    ActiveFilament(Filament), //AF
    DeviceAdress(usize),      //DA
    BaudRate(BRValue),        //BR
    AnalogOutput(DAC),        //DAC
    EmissionCurrent(ECValue), //EC
    FactoryDefault,           //FD
    RSDelayON(bool),          //RSD
    TestLedON(bool),          //TST
    PressureUnit(PUnit),      //U
    UserTag(Vec<u8, 12>),     //UT
    //status commands
    ClearUptimeValues,              //TIM2!CLR
    ActiveFilamentPowerON(bool), //FP  MUST QUERY PRESSURE. MAKE SURE P < 5e-2 Torr. Must Disable SetPoints
    DegasON(bool), //DG  MUST QUERY PRESSURE. MAKE SURE P<1e-5 Torr
    EnableSetPoint(SetPoint, bool), //ENx, ENC, PRO!ON, PRO!OFF
    SetPointValue(SetPoint, SciNumber),
    SetPointHysteresis(SetPoint, SciNumber), //hysteresis must be consistent with direction
    SetPointDirectionAbove(SetPoint, bool),
    //Calibration
    AtmosphericPressure(SciNumber), //ATM
    VacuumReadoutZero,              //VAC
    GasType(GasType),               //GT
    ManualGasCorrection(f32),       //GC, 0.10 to 50.1
}

/// Possible data types for data inside a response message
#[derive(Clone)]
pub enum Response {
    Boolean(bool),
    Float(f32),
    Text(Vec<u8, MAX_STR_LEN>),
    SciNumber(SciNumber),
    DeviceStatus(DeviceStatus),
}
impl Display for Response {
    fn fmt(&self, formatter: &mut Formatter<'_>) -> fmt::Result {
        match self {
            Response::Boolean(val) => write!(formatter, "{:?}", val),
            Response::Float(val) => write!(formatter, "{:.2E}", val),
            Response::Text(val) => {
                write!(formatter, "{:?}", val)
            }
            Response::SciNumber(val) => write!(formatter, "{}", val),
            Response::DeviceStatus(val) => write!(formatter, "{:?}", val),
        }
    }
}

impl Display for Setting {
    fn fmt(&self, formatter: &mut Formatter<'_>) -> fmt::Result {
        match self {
            Setting::ActiveFilament(filament) => match filament {
                Filament::One => write!(formatter, "AF!1"),
                Filament::Two => write!(formatter, "AF!2"),
            },
            Setting::DeviceAdress(addr) => write!(formatter, "DA!{:03}", addr),
            Setting::BaudRate(br) => write!(formatter, "BR!{}", br),
            Setting::AnalogOutput(dac) => match dac {
                DAC::DAC1 => write!(formatter, "DAC!1"),
                DAC::DAC2 => write!(formatter, "DAC!2"),
            },
            Setting::EmissionCurrent(ec) => match ec {
                ECValue::Manual20uA => write!(formatter, "EC!20UA"),
                ECValue::Auto => write!(formatter, "EC!AUTO"),
                _ => Err(fmt::Error), //DriverError::InvalidSettingParameter(parameter)),
            },
            Setting::FactoryDefault => write!(formatter, "FD!"),
            Setting::RSDelayON(turn_on) => {
                let state = if *turn_on { "ON" } else { "OFF" };
                write!(formatter, "RSD!{}", state)
            }
            Setting::TestLedON(turn_on) => {
                let state = if *turn_on { "ON" } else { "OFF" };
                write!(formatter, "TST!{}", state)
            }
            Setting::PressureUnit(unit) => match unit {
                PUnit::Torr => write!(formatter, "U!TORR"),
                PUnit::MiliBar => write!(formatter, "U!MBAR"),
                PUnit::Pascal => write!(formatter, "U!PASCAL"),
            },
            Setting::UserTag(tag) => write!(
                formatter,
                "UT!{}",
                from_utf8(tag).map_err(|_| fmt::Error)?
            ),
            Setting::ClearUptimeValues => write!(formatter, "TIM2!CLEAR"),
            Setting::ActiveFilamentPowerON(turn_on) => {
                let state = if *turn_on { "ON" } else { "OFF" };
                write!(formatter, "FP!{}", state)
            }
            Setting::DegasON(turn_on) => {
                let state = if *turn_on { "ON" } else { "OFF" };
                write!(formatter, "DG!{}", state)
            }
            Setting::EnableSetPoint(setpoint, enable) => {
                let status = if *enable { "ON" } else { "OFF" };
                match setpoint {
                    SetPoint::SP1 => write!(formatter, "EN1!{}", status),
                    SetPoint::SP2 => write!(formatter, "EN2!{}", status),
                    SetPoint::SP3 => write!(formatter, "EN3!{}", status),
                    SetPoint::ControluP => write!(formatter, "ENC!{}", status),
                    SetPoint::Protection => write!(formatter, "PRO!{}", status),
                }
            }
            Setting::SetPointValue(setpoint, value) => match setpoint {
                SetPoint::SP1 => write!(formatter, "SP1!{}", value),
                SetPoint::SP2 => write!(formatter, "SP2!{}", value),
                SetPoint::SP3 => write!(formatter, "SP3!{}", value),
                _ => Err(fmt::Error), //return Err(DriverError::NonConfigurableSetPoint(setpoint)),
            },
            Setting::SetPointHysteresis(setpoint, hysteresis) => match setpoint
            {
                SetPoint::SP1 => write!(formatter, "SH1!{}", hysteresis),
                SetPoint::SP2 => write!(formatter, "SH2!{}", hysteresis),
                SetPoint::SP3 => write!(formatter, "SH3!{}", hysteresis),
                _ => Err(fmt::Error), //return Err(DriverError::NonConfigurableSetPoint(setpoint)),
            },
            Setting::SetPointDirectionAbove(setpoint, direction_above) => {
                let value = if *direction_above { "ABOVE" } else { "BELOW" };
                match setpoint {
                    SetPoint::SP1 => write!(formatter, "SD1!{}", value),
                    SetPoint::SP2 => write!(formatter, "SD2!{}", value),
                    SetPoint::SP3 => write!(formatter, "SD3!{}", value),
                    _ => Err(fmt::Error), //return Err(DriverError::NonConfigurableSetPoint(setpoint))
                }
            }
            Setting::AtmosphericPressure(atm) => {
                write!(formatter, "ATM!{}", atm)
            }
            Setting::VacuumReadoutZero => write!(formatter, "VAC!"),
            Setting::GasType(gas_type) => match gas_type {
                GasType::Air => write!(formatter, "GT!AIR"),
                GasType::Nitrogen => write!(formatter, "GT!NITROGEN"),
                GasType::Argon => write!(formatter, "GT!ARGON"),
                GasType::Hydrogen => write!(formatter, "GT!HYDROGEN"),
                GasType::Helium => write!(formatter, "GT!HELIUM"),
                GasType::H2O => write!(formatter, "GT!H2O"),
                _ => write!(formatter, "GC!{}", gas_type),
            },
            Setting::ManualGasCorrection(value) => {
                if 0.09 < *value && *value < 50.2 {
                    if *value > 10.0 {
                        write!(formatter, "GC!{:.01}", value)
                    } else {
                        write!(formatter, "GC!{:.2}", value)
                    }
                } else {
                    Err(fmt::Error) // return Err(DriverError::InvalidSettingParameter(parameter));
                }
            }
        }
    }
}
/// Creates a valid value query string from the information provided by a `Value` object
impl Display for Value {
    fn fmt(&self, formatter: &mut Formatter<'_>) -> fmt::Result {
        match self {
            Value::ActiveFilament => write!(formatter, "AF?"),
            Value::DeviceAdress => write!(formatter, "DA?"),
            Value::BaudRate => write!(formatter, "BR?"),
            Value::AnalogOutput => write!(formatter, "DAC?"),
            Value::EmissionCurrent => write!(formatter, "EC?"),
            Value::RSDelayON => write!(formatter, "RSD?"),
            Value::TestLedON => write!(formatter, "TST?"),
            Value::PressureUnit => write!(formatter, "U?"),
            Value::UserTag => write!(formatter, "UT?"),
            Value::DeviceType => write!(formatter, "DT?"),
            Value::ActiveFilamentON => write!(formatter, "FS?"),
            Value::FirmwareVersion => write!(formatter, "FV?"),
            Value::HardwareVersionMicroPirani => write!(formatter, "HV?"),
            Value::Manufacturer => write!(formatter, "MF?"),
            Value::Model => write!(formatter, "MD?"),
            Value::SerialNumber => write!(formatter, "SN?"),
            Value::DeviceUptime => write!(formatter, "TIM1?"),
            Value::FilamentUptime => write!(formatter, "TIM2?"),
            Value::DeviceStatus => write!(formatter, "T?"),
            Value::OnChipSensorTemp => write!(formatter, "TEM1?"),
            Value::HotCathodeTemp => write!(formatter, "TEM2?"),
            Value::ActiveFilamentPowerON => write!(formatter, "FP?"),
            Value::DegasON => write!(formatter, "DG?"),
            Value::PressureReadingMicroPirani => write!(formatter, "PR1?"),
            Value::PressureReadingHotCathode => write!(formatter, "PR2?"),
            Value::PressureReadingCombined => write!(formatter, "PR3?"),
            Value::SetPointEnabled(setpoint) => match setpoint {
                SetPoint::SP1 => write!(formatter, "EN1?"),
                SetPoint::SP2 => write!(formatter, "EN2?"),
                SetPoint::SP3 => write!(formatter, "EN3?"),
                SetPoint::ControluP => write!(formatter, "ENC?"),
                SetPoint::Protection => write!(formatter, "PRO?"),
            },
            Value::SetPointValue(setpoint) => match setpoint {
                SetPoint::SP1 => write!(formatter, "SP1?"),
                SetPoint::SP2 => write!(formatter, "SP2?"),
                SetPoint::SP3 => write!(formatter, "SP3?"),
                _ => Err(fmt::Error), //return Err(DriverError::InvalidValueParameter(parameter)),
            },
            Value::SetPointHysteresis(setpoint) => match setpoint {
                SetPoint::SP1 => write!(formatter, "SH1?"),
                SetPoint::SP2 => write!(formatter, "SH2?"),
                SetPoint::SP3 => write!(formatter, "SH3?"),
                _ => Err(fmt::Error), //return Err(DriverError::InvalidValueParameter(parameter)),
            },
            Value::SetPointDirection(setpoint) => match setpoint {
                SetPoint::SP1 => write!(formatter, "SD1?"),
                SetPoint::SP2 => write!(formatter, "SD2?"),
                SetPoint::SP3 => write!(formatter, "SD3?"),
                _ => Err(fmt::Error), //return Err(DriverError::InvalidValueParameter(parameter)),
            },
            Value::SetPointSetOrClear(setpoint) => match setpoint {
                SetPoint::SP1 => write!(formatter, "SS1?"),
                SetPoint::SP2 => write!(formatter, "SS2?"),
                SetPoint::SP3 => write!(formatter, "SS3?"),
                _ => Err(fmt::Error), //return Err(DriverError::InvalidValueParameter(parameter)),
            },
            Value::ConfiguredGasType => write!(formatter, "GT?"),
            Value::ConfiguredGasCorrection => write!(formatter, "GC?"),
        }
    }
}
