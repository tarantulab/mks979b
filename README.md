# MKS 979B Atmosphere to Vacuum Transducer driver

## Intro

A Platform agnostic, `no_std`, serial communication driver for the MKS 979B Atmosphere to Vacuum Transducer, based on the embedded-hal traits.

- Implements the RS485/RS232 communication protocol specification for the MKS 979B
- Works in standalone use and via MKS PDR 900 Series controllers.

## Status

The driver is technically complete, but some further testing is required for some commands as to verify nothing behaves in a weird way.
All commands for the 979B Transducer are implemented. PDR 900 commands are out of scope for this crate.

## Usage

An simple blinky example is provided for the ItsyBitsy_m4. The API is quite straight forward.

### Why not use type states for the query functionality?

Type states were considered at some point but it sacrifices some comfiness in certain use-cases. This just means the user must make sure the sequence `schedule_message`-`send_message`-`poll` is correctly followed.

## License

Licensed under Apache-2.0.

### Contributions

Unless you explicitly state otherwise, any contribution intentionally submitted
for inclusion in this work by you shall be under the terms and conditions of
the Apache-2.0 license, without any additional terms or conditions.

## Additional info

_Ideal Vacuum_ provides some important documentation for the instrument [here](https://www.idealvac.com/MKS-HPS-Series-979B-Atmosphere-to-Vacuum-Transducer/pp/P1011977).

![Image of the MKS 979B](https://www.idealvac.com/files/images/MKS-Transducer_979B-31_01.jpg)
